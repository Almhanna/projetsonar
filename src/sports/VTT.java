package sports;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "VTT")
public class VTT extends Sport {

	public VTT() {
		prix = 12F;
	}

}
