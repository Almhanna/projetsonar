package sports;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Canoe")
public class Canoe extends Sport {

	public Canoe() {
		prix = 12F;
	}

}
