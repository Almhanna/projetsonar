package sports;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Tennis")
public class Tennis extends Sport {

	public Tennis() {
		prix = 10F;
	}

}
