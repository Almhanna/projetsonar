package sports;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PlancheAVoile")
public class PlanchAVoile extends Sport {

	public PlanchAVoile() {
		prix = 16F;
	}

}
