package sports;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Pedalos")
public class Pedalo extends Sport {

	public Pedalo() {
		prix = 7F;
	}

}
