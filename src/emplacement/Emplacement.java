package emplacement;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import classesGenerals.Camping;
import classesGenerals.Nuitee;

@Entity
@Table(name = "Emplacement")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Emplacement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	protected String numero;
	protected int capacite;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Camping getCamping() {
		return camping;
	}

	public void setCamping(Camping camping) {
		this.camping = camping;
	}

	protected int surface;
	protected float prix;

	@ManyToOne()
	@JoinColumn(name = "camping_id")
	private Camping camping;

	@OneToMany(mappedBy = "emplacement")
	private List<Nuitee> nuitees;

	public List<Nuitee> getNuitees() {
		return nuitees;
	}

	public void setNuitees(List<Nuitee> nuitees) {
		this.nuitees = nuitees;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	public int getSurface() {
		return surface;
	}

	public void setSurface(int surface) {
		this.surface = surface;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

}
