package emplacement;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CampingCar")
public class CampingCar extends Emplacement {

	public CampingCar() {
		prix = 14F;
	}

}
