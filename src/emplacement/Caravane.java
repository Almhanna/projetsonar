package emplacement;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Caravane")
public class Caravane extends Emplacement {

	public Caravane() {
		prix = 13.5F;
	}

}
