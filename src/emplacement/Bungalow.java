package emplacement;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Bungalow")
public class Bungalow extends Emplacement {

	public Bungalow() {
		prix = 17.5F;
	}

}
