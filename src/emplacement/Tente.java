package emplacement;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Tente")
public class Tente extends Emplacement {

	public Tente() {
		prix = 11f;
	}

}
