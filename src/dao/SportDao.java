package dao;

import classesGenerals.Camping;

public class SportDao extends GenericDao<Camping> {

	public SportDao() {
		super(Camping.class);
	}

}
