package dao;

import classesGenerals.Camping;

public class JdbcSportDao extends GenericDao<Camping> {

	public JdbcSportDao() {
		super(Camping.class);
	}

}
