package dao;

import classesGenerals.Activite;

public class ActiviteDao extends GenericDao<Activite> {

	public ActiviteDao() {
		super(Activite.class);
	}

}
