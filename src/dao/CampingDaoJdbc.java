package dao;

import classesGenerals.Camping;

public class CampingDaoJdbc extends GenericDao<Camping> {

	public CampingDaoJdbc() {
		super(Camping.class);
	}

}
