package dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import emplacement.Emplacement;

public class TestConnection {
	public static void main(String[] args) throws Exception {
		listerEmplacement();

	}

	private static void listerEmplacement() {
		Session session = HibernateUtile.getInstance();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

		CriteriaQuery<Emplacement> criteria = criteriaBuilder.createQuery(Emplacement.class);

		Root<Emplacement> root = criteria.from(Emplacement.class);
		criteria.select(root);
		Query<Emplacement> query = session.createQuery(criteria);
		List<Emplacement> list = query.getResultList();
		for (Emplacement emplacment : list) {
			System.out.println(emplacment.getId() + " " + emplacment.getCapacite() + "(" + emplacment.getNumero()
					+ ") prix est " + emplacment.getPrix());

		}

	}

}
