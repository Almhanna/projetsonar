package dao;

import classesGenerals.Camping;

public interface ICampingDao<E> extends IGenericDao<Camping> {

}
