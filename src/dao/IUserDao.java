package dao;

public interface IUserDao {
	public void recherche();

	public boolean exister();

	public void insert();

	public Long nouveauFil(String topic);

	public void nouveauMessage(String text, Long id);

}
