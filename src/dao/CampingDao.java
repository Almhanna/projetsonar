package dao;

import classesGenerals.Camping;

public class CampingDao extends GenericDao<Camping> {

	public CampingDao() {
		super(Camping.class);
	}

}
