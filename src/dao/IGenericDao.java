package dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao<E> {
	public E getById(Serializable id);

	public Serializable save(E e);

	public List<E> getAll();

}
