package dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public abstract class JdbcGenericDao<E> implements IGenericDao<E> {
	private Class<E> clazz;

	public JdbcGenericDao(Class<E> clazz) {
		this.clazz = clazz;
	}

	public E getById(Serializable id) {
		Session session = HibernateUtile.getInstance();
		return session.load(clazz, id);
	}

	public Serializable save(E e) {
		Session session = HibernateUtile.getInstance();
		Transaction tx = session.beginTransaction();
		Serializable result = session.save(e);
		tx.commit();
		return result;
	}

	public List<E> getAll() {
		Session session = HibernateUtile.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<E> criteria = criteriaBuilder.createQuery(clazz);
		Root<E> root = criteria.from(clazz);
		Query<E> query = session.createQuery(criteria);
		return query.getResultList();
	}

}
