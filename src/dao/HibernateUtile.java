package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtile {
	private static Session instance = null;

	public static Session getInstance() {
		if (instance == null) {
			SessionFactory sessionFactory = new Configuration().configure("resources/hibernate.cfg.xml")
					.buildSessionFactory();
			instance = sessionFactory.openSession();

		}
		return instance;
	}

}
