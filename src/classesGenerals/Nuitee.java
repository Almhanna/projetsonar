package classesGenerals;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import emplacement.Emplacement;

@Entity
@Table(name = "Nuitee")
public class Nuitee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private Date date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setEmplacement(Emplacement emplacement) {
		this.emplacement = emplacement;
	}

	@ManyToOne()
	@JoinColumn(name = "emplacement_id")
	private Emplacement emplacement;

	@ManyToOne()
	@JoinColumn(name = "sejour_id")
	private Sejour sejour;

	public Sejour getSejour() {
		return sejour;
	}

	public void setSejour(Sejour sejour) {
		this.sejour = sejour;
	}

	public Emplacement getEmplacement() {
		return emplacement;
	}

	public void setEmplacment(Emplacement emplacement) {
		this.emplacement = emplacement;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
