package classesGenerals;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Sejour")
public class Sejour {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String numero;

	@OneToOne
	@JoinColumn(name = "groupe_id")
	private Groupe groupe;

	@OneToOne
	@JoinColumn(name = "facture_id")
	private Facture facture;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@OneToMany(mappedBy = "sejour")
	private List<Activite> activites;

	@OneToMany(mappedBy = "sejour")
	private List<Nuitee> nuitees;

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public Facture getFacture() {
		return facture;
	}

	public void setFacture(Facture facture) {
		this.facture = facture;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	public List<Nuitee> getNuitees() {
		return nuitees;
	}

	public void setNuitees(List<Nuitee> nuitees) {
		this.nuitees = nuitees;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
