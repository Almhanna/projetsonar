package classesGenerals;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Groupe")
public class Groupe {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String nom;
	private String prenom;
	private int personnes;

	@OneToOne
	@JoinColumn(name = "sejour_id")
	private Sejour sejour;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Sejour getSejour() {
		return sejour;
	}

	public void setSejour(Sejour sejour) {
		this.sejour = sejour;
	}

	public void setPersonnes(int personnes) {
		this.personnes = personnes;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getPersonnes() {
		return personnes;
	}

	public void setPersonne(int personnes) {
		this.personnes = personnes;
	}

}
