package classesGenerals;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import sports.Sport;

@Entity
@Table(name = "Activite")
public class Activite {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "date")
	private Date date;

	@Column(name = "nombre")
	private int nbrUnite;

	@ManyToOne()
	@JoinColumn(name = "sport_id")
	private Sport sport;

	@ManyToOne()
	@JoinColumn(name = "sejour_id")
	private Sejour sejour;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Sport getSport() {
		return sport;
	}

	public void setSport(Sport sport) {
		this.sport = sport;
	}

	public Sejour getSejour() {
		return sejour;
	}

	public void setSejour(Sejour sejour) {
		this.sejour = sejour;
	}

	public int getNbrUnite() {
		return nbrUnite;
	}

	public void setNbrUnite(int nbrUnite) {
		this.nbrUnite = nbrUnite;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
