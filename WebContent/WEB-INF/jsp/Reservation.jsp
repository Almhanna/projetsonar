<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb"
	lang="en-gb">
<head>
<meta charset="utf-8">
	<title>Reservation</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<div class="super">
		<div class="corps">
			<div class="entete">
				<h1>Groupe CVE - R�servation</h1>
				<div class="phototexte">
					<img class="photo" src="images/camping.jpg">
					<div class="texte">
						<p>Ce site est un service qui vous permet de reserver en
							ligne et en temps r�el, votre s�jour dans un camping</p>
						<p>Simple et s�curitaire : ce site est la solution facile
							pour planifier vos vacances ou vos week-ends dans la r�gion de
							votre choix</p>
					</div>
				</div>
				<form method="post" action="formulaire">
					<div class="formulaire">
						<div class="zone1">
							<div class="region">
								<p>
									<em>1 </em> CHOISIR UNE REGION :
								</p>
								<form class="listeregion">
									<select name="region" id ="listeRegion"  size="1">
										<optgroup label="Toutes les r�gions">
											<option>TOUTES LES REGIONS</option>
											<option>Bretagne</option>
											<option>Vend�e</option>
										</optgroup>
									</select>
								</form>
							</div>
							<form class="dateArrivee">
								<p>
									<em>2 </em> DATE D'ARRIVEE :
								</p>
								<input name="date" id="date" type="date" />
							</form>


							<form class="nuitee">
								<p>
									<em>3 </em> NOMBRE DE NUITEE :
								</p>
								<input id="nuitee" type="number" name="quantity" min="0"
									max="100" value="1" input type="niveau" />
							</form>



							<form class="equipement">
								<p>
									<em>5 </em> TYPE D'EQUIPEMENT :
								</p>
								<div>
									<div class="listeequipement">
									  <input type="radio" id="tente" name="equipement" value="huey">
									  <label for="tente">Tente</label>
									</div>
									<div>
									  <input type="radio" id="caravane" name="equipement" value="dewey">
									  <label for="caravane">Caravane</label>
									</div>
									
									<div>
									  <input type="radio" id="campingcar" name="equipement" value="louie">
									  <label for="campingcar">Camping Car</label>
									</div>
									<div>
									  <input type="radio" id="bungalow" name="equipement" value="louie">
									  <label for="bungalow">Bungalow</label>
									</div>
								</div>
							</form>
						

						</div>
					</div>
					<button type="summit">D�buter la recherche</button>
				</form>
			</div>



		</div>
</body>
</html>